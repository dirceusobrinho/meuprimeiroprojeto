package br.com.dirceusobrinho.meuprojeto.aplicacao;

import java.util.List;

import br.com.dirceusobrinho.meuprojeto.dominio.modelo.noticia.Noticia;

public interface NoticiaService {

	public void salvarNoticia(Noticia noticia);
	
	public List<Noticia> pesquisarNoticiaPorTitulo(String titulo);
	
	public List<Noticia> ultimasNoticias();
	
	public void removerNoticia(int id);
	
}
