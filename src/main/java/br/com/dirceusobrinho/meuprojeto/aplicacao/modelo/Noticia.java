package br.com.dirceusobrinho.meuprojeto.aplicacao.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="noticia")
public class Noticia {

	@Id
	private Integer id;
	
	private String titulo;
	
	private String conteudo;
	
	public Noticia() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	@Override
	public String toString() {
		return "Notícia [ "+titulo+" ]";
	}
	
	
	
	
}
