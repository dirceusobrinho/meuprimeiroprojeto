package br.com.dirceusobrinho.meuprojeto.aplicacao.servicos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dirceusobrinho.meuprojeto.aplicacao.modelo.Noticia;
import br.com.dirceusobrinho.meuprojeto.infraestrutura.repositorio.NoticiaDao;

@Service
public class NoticiaServiceImpl implements NoticiaService{

	@Autowired
	NoticiaDao noticiaDao;
	
	public void salvarNoticia(Noticia noticia) {
		noticiaDao.save(noticia);
	}
	
	public List<Noticia> pesquisarNoticiaPorTitulo(String titulo) {
		return noticiaDao.findByTitulo(titulo);
	}
	
	public List<Noticia> ultimasNoticias() {
		return noticiaDao.findAll();
	}
	
	public void removerNoticia(int id) {
		noticiaDao.deleteById(id);
	}
	
}
