package br.com.dirceusobrinho.meuprojeto.infraestrutura.repositorio;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.dirceusobrinho.meuprojeto.dominio.modelo.noticia.Noticia;

@Repository
public interface NoticiaDao extends MongoRepository<Noticia, Integer>{

	List<Noticia> findByTitulo(String titulo);

}
