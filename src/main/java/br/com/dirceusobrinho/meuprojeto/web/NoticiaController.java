package br.com.dirceusobrinho.meuprojeto.web;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dirceusobrinho.meuprojeto.aplicacao.NoticiaService;
import br.com.dirceusobrinho.meuprojeto.dominio.modelo.noticia.Noticia;

@RestController
@RequestMapping(value="/api/noticia")
public class NoticiaController {

		@Autowired
		NoticiaService noticiaService;
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@PostMapping(value="/salvar")
		public String salvar(@RequestBody Noticia noticia) {
			logger.debug("Salvando notícia");
			noticiaService.salvarNoticia(noticia);
			return "Notícia Salva";
		}
		
		@GetMapping(value="/ultimas")
		public Collection<Noticia> ultimasNoticias(){
			logger.debug("Retornando as últimas notícias");
			return noticiaService.ultimasNoticias();
		}
	
		@GetMapping(value="/pesquisa/{titulo}")
		public Collection<Noticia> pesquisaPorTitulo(@PathVariable(value="titulo") String titulo){
			return noticiaService.pesquisarNoticiaPorTitulo(titulo);
		}
		
		@DeleteMapping(value="/remover/{id}")
		public String removerNoticia(@PathVariable(value="id") int id) {
			noticiaService.removerNoticia(id);
			return "Removida notícia de código: "+ id;
		}
		
	
	
}
